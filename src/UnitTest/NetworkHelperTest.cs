﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DataAccess;
using Xunit;
using Utility;
namespace UnitTest
{
    public class NetworkHelperTest
    {
        [Fact]
        public void TestGet()
        {
            var response = NetworkHelper.Get("http://www.baidu.com");
            
            Assert.True(response.StatusCode==HttpStatusCode.OK);
        }

        [Fact]
        public void TestPostWithFile()
        {
            //var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url =
                $"https://api.weixin.qq.com/cgi-bin/media/upload?access_token={DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken().access_token}&type=image";
            var fs=new FileStream("C:\\Users\\Kenny.Li\\Pictures\\test.jpg", FileMode.Open);
            var response = NetworkHelper.PostWithMediaFile(url, fs, "image","jpg","media",null);
            var responseData = response.GetResponseData();

            Assert.NotNull(response);
        }
        [Fact]
        public void TestPostWithForeverVideoFile()
        {
            //var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url =
                $"https://api.weixin.qq.com/cgi-bin/material/add_material?access_token={DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken().access_token}";
            var fs = new FileStream("C:\\Users\\Kenny.Li\\Pictures\\test.mp4", FileMode.Open);
            Dictionary<string,string> dic=new Dictionary<string, string>();
            dic.Add("description", "{\"title\":\"Mouse\",\"introduction\":\"MouseIntroduction\"}");
            var response = NetworkHelper.PostWithMediaFile(url, fs, "video", "mp4", "media", dic);
            var responseData = response.GetResponseData();

            Assert.NotNull(response);
        }
    }
}
