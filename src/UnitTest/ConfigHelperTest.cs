﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Utility;
namespace UnitTest
{
    public class ConfigHelperTest
    {
        [Fact]
        public void TestGetValue()
        {
            var token = ConfigHelper.WechatToken;
            Assert.Equal(token, "123456");
        }
    }
}
