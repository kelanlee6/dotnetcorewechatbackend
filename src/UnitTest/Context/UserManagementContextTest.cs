﻿using Context;
using Xunit;
namespace UnitTest
{
    public class UserManagementContextTest
    {
        [Fact]
        public void SaveOrUpdateWechatUserTest()
        {
            var result=ContextManageFactory.CreateUserManagementContext().SaveOrUpdateWechatUser("oKWGOuBpYKDLi2kxTlLDWsEC9jRM");
            Assert.True(result);
        }

        [Fact]
        public void UpdateAllUserTest()
        {
            var result = ContextManageFactory.CreateUserManagementContext().UpdateAllWechatUser();
            Assert.True(result);
        }

    }
}
