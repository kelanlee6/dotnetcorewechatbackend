﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Database.Entities;
using Xunit;
using Utility;
namespace UnitTest
{
    public class DataAccessTest
    {
        [Fact]
        public void CreateTest()
        {
            var user=new WechatUser();
            user.subscribe = 1;
            user.openid = new Guid().ToString();
            user.nickname = "123";
            user.sex = 1;
            user.city = "123";
            user.country = "123";
            user.province = "123";
            user.language = "123";
            user.headimgurl = "123";
            user.subscribe_time = 1234567;
            user.unionid = "123";
            user.remark = "123";
            user.groupid = 1;
            user.tagid_list = new List<int>() {1,2,3,4};
            var str = user.tagid_listStr;
            var result = DataAccessManageFactory.CreateWechatUserRepository().Create(user);
            Assert.NotNull(result);
        }

        [Fact]
        public void GetAndDeleteTest()
        {
            var user = DataAccessManageFactory.CreateWechatUserRepository().Find(m=>m.openid==new Guid().ToString());
            var str1 = user.tagid_listStr;
            var list = user.tagid_list;
            DataAccessManageFactory.CreateWechatUserRepository().Delete(user);
            Assert.True(true);
        }
        
    }
}
