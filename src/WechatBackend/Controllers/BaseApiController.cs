﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace WechatBackend.Controllers
{
    [Route("api/[controller]")]
    public class BaseApiController : Controller
    {
        public BaseApiController()
        {
            Logger = LogManager.GetLogger(GetType().FullName);
        }

        protected Logger Logger { get; private set; }

        protected IActionResult ResponseSuccessfulMessage<T>(T result)
        {
            return new OkObjectResult(result);
        }

        protected IActionResult ResponseFailedMessage()
        {
            return new BadRequestObjectResult("400:BadRequest");
        }

        protected IActionResult ResponseExceptionMessage()
        {
            var result=new ObjectResult("500:InternalServerError");
            result.StatusCode = 500;
            return result;
        }

        protected IActionResult ResponseUnauthenticatedMessage()
        {
            return new UnauthorizedResult();
        }
    }

    public class result : IActionResult
    {
        public Task ExecuteResultAsync(ActionContext context)
        {
            throw new NotImplementedException();
        }
    }
}
