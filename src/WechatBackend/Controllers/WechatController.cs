﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Models;
using Services;
using Utility;
using LogLevel = NLog.LogLevel;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.MP.Sample.CommonService.CustomMessageHandler;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WechatBackend.Controllers
{
    [Route("api/[controller]")]
    public class WechatController : BaseApiController
    {
        private readonly IMessageService _messageService=ServiceManagerFactory.CreateMessageService();
        
        [HttpGet]
        public IActionResult Get(string signature,string timestamp,string nonce,string echostr)
        {
            string token = Utility.ConfigHelper.WechatToken;
            //Logger.Log(LogLevel.Debug,$"wechat Token :{token}");
            //string[] arry = {token, timestamp, nonce};
            //Array.Sort(arry);
            //string tmpStr = string.Join("", arry);
            //tmpStr = SHA1Encode(tmpStr);
            ////tmpStr = Security.FormsAuthentication.HashPasswordForStoringInConfigFile(tmpStr, "SHA1");
            //tmpStr = tmpStr.ToLower();
            //if (tmpStr==signature)
            //{
            //    Logger.Log(LogLevel.Debug, $"Auth Successed");
            //    return ResponseSuccessfulMessage(echostr);
            //}
            //Logger.Log(LogLevel.Debug, $"Auth Failed");
            //return ResponseUnauthenticatedMessage();
            if (CheckSignature.Check(signature, timestamp, nonce, token))
            {
                return Content(echostr); //返回随机字符串则表示验证通过
            }
            else
            {
                return Content("failed:" + signature + "," + CheckSignature.GetSignature(timestamp, nonce, token) + "。" +
                    "如果你在浏览器中看到这句话，说明此地址可以被作为微信公众账号后台的Url，请注意保持Token一致。");
            }
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post(string signature, string timestamp, string nonce)
        {
            var context = Request.HttpContext;
            var stream = context.Request.Body;
            var sw = new StreamReader(stream);
            var postStr = sw.ReadToEnd();
            XDocument xDocument=XDocument.Parse(postStr);
            //if (!string.IsNullOrEmpty(postStr))
            //{
            //    Logger.Log(LogLevel.Debug, "Access API.");
            //    var message = _messageService.AnalysisMessageFromXmlStr(postStr);
            //    if (message != null)
            //    {
            //        var isStore=_messageService.StoreMessage(message);
            //        Logger.Info(isStore?$"Already store {message.MsgType} type Message": $"Failed to store {message.MsgType} type Message");
            //        Logger.Log(LogLevel.Debug, "Get Message successed.");
            //        var xmlStr = String.Empty;
            //        MessageModel responseMsg;
            //        if (ConfigHelper.RepeatReply)
            //        {
            //             responseMsg= message.CreateRepeatResponeMessage();
            //        }
            //        else
            //        {
            //            responseMsg=message.CreateResponseTextMessage("收到。");
            //        }
            //        if (responseMsg!=null)
            //        {
            //            _messageService.StoreMessage(responseMsg);
            //            xmlStr = responseMsg.ToXml();
            //        }
            //        Logger.Debug("reply message:\n"+xmlStr);

            //        return ResponseSuccessfulMessage(xmlStr);
            //    }
            //}
            //Logger.Log(LogLevel.Debug, "Get Message failed.");
            //return ResponseSuccessfulMessage("failed");
            string token = ConfigHelper.WechatToken;
            if (!CheckSignature.Check(signature, timestamp, nonce, token))
            {
                //return Content("参数错误！");//v0.7-
                return Content("参数错误！");//v0.8+
            }
            PostModel postModel=new PostModel();
            postModel.Signature = signature;
            postModel.Nonce = nonce;
            postModel.Timestamp = timestamp;
            postModel.Token = token;
            postModel.EncodingAESKey = "";//根据自己后台的设置保持一致
            postModel.AppId = ConfigHelper.WechatAppId;//根据自己后台的设置保持一致
            var messageHandler = new CustomMessageHandler(xDocument, postModel, 10);

            messageHandler.Execute();//执行微信处理过程

            return Content(messageHandler.ResponseDocument.ToString());//v0.7-
        }

        //private static string SHA1Encode(string text)
        //{
        //    byte[] cleanBytes = Encoding.UTF8.GetBytes(text);
        //    byte[] hashedBytes =SHA1.Create().ComputeHash(cleanBytes);
        //    return BitConverter.ToString(hashedBytes).Replace("-", "");
        //}
    }
}
