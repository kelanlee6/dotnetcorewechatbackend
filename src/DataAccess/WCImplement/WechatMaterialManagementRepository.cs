﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using DataAccess.DBInterface;
using DataAccess.WCInterface;
using Newtonsoft.Json;
using Utility;

namespace DataAccess.WCImplement
{
    class WechatMaterialManagementRepository : BaseWechatRepository, IWechatMaterialManagementRepository
    {
        private readonly IWechatTokenRepository _wechatTokenRepository;
        public WechatMaterialManagementRepository()
        {
            _wechatTokenRepository = DataAccessManageFactory.CreateWechatTokenRepository();
        }

        #region TemporaryMaterial

        public TemporaryMaterialResult AddTemporaryVideoMaterial(string filePath)
        {
            return AddTemporaryMaterial(filePath, "video");
        }

        public TemporaryMaterialResult AddTemporaryVoiceMaterial(string filePath)
        {

            return AddTemporaryMaterial(filePath, "voice");
        }

        public TemporaryMaterialResult AddTemporaryImageMaterial(string filePath)
        {
            return AddTemporaryMaterial(filePath, "image");
        }

        public TemporaryMaterialResult AddTemporaryThumbMaterial(string filePath)
        {
            return AddTemporaryMaterial(filePath, "thumb");
        }

        public Stream GetMaterial(string mediaId, out string fileName, out int length)
        {
            fileName = string.Empty;
            length = 0;
            var token = _wechatTokenRepository.GetCurrentToken();
            var url = ConfigHelper.WetchatDomain +
                      $"/cgi-bin/media/get?access_token={token.access_token}&media_id={mediaId}";
            var response = NetworkHelper.Get(url);
            var stream = response.GetResponseStream();
            int.TryParse(response.Headers["Content-Length"], out length);
            byte[] tmpBuffer = new byte[1024];
            List<byte> buffer = new List<byte>();
            var index = 0;
            while ((index = stream.Read(tmpBuffer, 0, tmpBuffer.Length)) != 0)
            {
                buffer.AddRange(tmpBuffer.ToList().Take(index));
            }

            var tmpStr = Encoding.UTF8.GetString(buffer.ToArray());
            var error = CheckError(tmpStr);
            if (error.HasError)
            {
                return null;
            }
            var contentdisposition = response.Headers["Content-disposition"];
            if (contentdisposition != null)
            {
                var startIndex = contentdisposition.LastIndexOf(mediaId, StringComparison.CurrentCulture);
                fileName = contentdisposition.Substring(startIndex).TrimEnd('"');
            }
            Stream result = new MemoryStream(buffer.ToArray());
            return result;
        }

        #endregion

        #region ForeverMaterial

        public ForeverMaterialResult AddForverVideoMaterial(string filePath)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("description", "{\"title\":\"Mouse\",\"introduction\":\"MouseIntroduction\"}");
            return AddForverMaterial(filePath, "video", dic);
        }

        public ForeverMaterialResult AddForverVoiceMaterial(string filePath)
        {
            return AddForverMaterial(filePath, "voice");
        }

        public ForeverMaterialResult AddForverImageMaterial(string filePath)
        {
            return AddForverMaterial(filePath, "image");
        }

        public ForeverMaterialResult AddForverThumbMaterial(string filePath)
        {
            return AddForverMaterial(filePath, "thumb");
        }

        public URLResult AddImageForImageTextMaterial(string filePath)
        {
            var token = _wechatTokenRepository.GetCurrentToken();
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var ext = Path.GetExtension(filePath);
            FileStream fs = new FileStream(filePath, FileMode.Open);
            var url = ConfigHelper.WetchatDomain +
                      $"/cgi-bin/media/uploadimg?access_token={token.access_token}";
            var response = NetworkHelper.PostWithMediaFile(url, fs, fileName, ext, "image", null);
            var responseData = response.GetResponseData();
            var error = CheckError(responseData);
            if (error.HasError)
            {
                return null;
            }
            return JsonConvert.DeserializeObject<URLResult>(responseData);
        }

        public ForeverMaterialResult AddForverImageTextMaterial(AddImageTextMaterialRequest data)
        {
            var token = _wechatTokenRepository.GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + $"/cgi-bin/material/add_news?access_token={token.access_token}";
            var jsonData = JsonConvert.SerializeObject(data);
            var response = NetworkHelper.Post(url, jsonData);
            return JsonConvert.DeserializeObject<ForeverMaterialResult>(response.GetResponseData());
        }
        public ForeverMaterialResult AddImageTextMessageMaterial(AddImageTextMaterialRequest data)
        {
            var token = _wechatTokenRepository.GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + $"/cgi-bin/material/uploadnews?access_token={token.access_token}";
            var jsonData = JsonConvert.SerializeObject(data);
            var response = NetworkHelper.Post(url, jsonData);
            return JsonConvert.DeserializeObject<ForeverMaterialResult>(response.GetResponseData());
        }
        public ForeverVideoMaterialResult GetForeverVideoMaterial(string mediaId)
        {
            return
                JsonConvert.DeserializeObject<ForeverVideoMaterialResult>(GetForeverMaterial(mediaId).GetResponseData());
        }

        public Stream GetForeverVoiceMaterial(string mediaId)
        {
            var response = GetForeverMaterial(mediaId);
            return GetStreamFromNetworkStream(response.GetResponseStream());
        }

        public Stream GetForeverImageMaterial(string mediaId)
        {
            var response = GetForeverMaterial(mediaId);
            return GetStreamFromNetworkStream(response.GetResponseStream());
        }

        public Stream GetForeverThumbMaterial(string mediaId)
        {
            var response = GetForeverMaterial(mediaId);
            return GetStreamFromNetworkStream(response.GetResponseStream());
        }

        public ImageTextMaterialResult GetForeverImageTextMaterial(string mediaId)
        {
            return
                JsonConvert.DeserializeObject<ImageTextMaterialResult>(GetForeverMaterial(mediaId).GetResponseData());
        }

        public bool DeleteForeverMaterial(string mediaId)
        {
            var token = _wechatTokenRepository.GetCurrentToken();
            var url = ConfigHelper.WetchatDomain +
                      $"/cgi-bin/material/del_material?access_token={token.access_token}";
            var jsonData = "{\"media_id\":\"" + mediaId + "\"}";
            var response=NetworkHelper.Post(url, jsonData);
            var error = CheckError(response.GetResponseData());
            //todo throw exception if has error
            return !error.HasError;
        }

        public bool UpdateImageTextMaterial(UpdateImageTextMaterialRequest data)
        {
            var token = _wechatTokenRepository.GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + $"/cgi-bin/material/update_news?access_token={token.access_token}";
            var jsonData = JsonConvert.SerializeObject(data);
            var response = NetworkHelper.Post(url, jsonData);
            var error = CheckError(response.GetResponseData());
            //todo throw exception if has error
            return !error.HasError;
        }

        public MaterialCount GetMaterialCount()
        {
            var token = _wechatTokenRepository.GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + $"/cgi-bin/material/get_materialcount?access_token={token.access_token}";
            var response = NetworkHelper.Get(url);
            var responseData = response.GetResponseData();
            var error = CheckError(responseData);
            if (error.HasError)
            {
                //todo throw exception if has error
                return null;
            }
            return JsonConvert.DeserializeObject<MaterialCount>(responseData);
        }

        public ForeverImageTextMaterialList GetImagrForeverImageTextMaterialList(int offset = 0, int count = 20)
        {
            var response = GetForeverMaterialListRequest("news", offset, count);
            var responseStr = response.GetResponseData();
            var error = CheckError(responseStr);
            if (error.HasError)
            {
                //todo throw exception if has error
                return null;
            }
            return JsonConvert.DeserializeObject<ForeverImageTextMaterialList>(responseStr);
        }

        public ForeverMaterialList GetForeverImageMaterialList(int offset = 0, int count = 20)
        {
            
            var response = GetForeverMaterialListRequest("image", offset, count);
            var responseStr = response.GetResponseData();
            var error = CheckError(responseStr);
            if (error.HasError)
            {
                //todo throw exception if has error
                return null;
            }
            return JsonConvert.DeserializeObject<ForeverMaterialList>(responseStr);
        }

        public ForeverMaterialList GetForeverVideoMaterialList(int offset = 0, int count = 20)
        {
            var response = GetForeverMaterialListRequest("video", offset, count);
            var responseStr = response.GetResponseData();
            var error = CheckError(responseStr);
            if (error.HasError)
            {
                //todo throw exception if has error
                return null;
            }
            return JsonConvert.DeserializeObject<ForeverMaterialList>(responseStr);
        }

        public ForeverMaterialList GetForeverVoiceMaterialList(int offset = 0, int count = 20)
        {
            var response = GetForeverMaterialListRequest("voice", offset, count);
            var responseStr = response.GetResponseData();
            var error = CheckError(responseStr);
            if (error.HasError)
            {
                //todo throw exception if has error
                return null;
            }
            return JsonConvert.DeserializeObject<ForeverMaterialList>(responseStr);
        }

        public ForeverMaterialList GetForeverThumbMaterialList(int offset = 0, int count = 20)
        {
            var response = GetForeverMaterialListRequest("thumb", offset, count);
            var responseStr = response.GetResponseData();
            var error = CheckError(responseStr);
            if (error.HasError)
            {
                //todo throw exception if has error
                return null;
            }
            return JsonConvert.DeserializeObject<ForeverMaterialList>(responseStr);
        }

        #endregion



        #region CommonFunctions

        public HttpWebResponse GetForeverMaterialListRequest(string type,int offset,int count)
        {
            var token = _wechatTokenRepository.GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + $"/cgi-bin/material/batchget_material?access_token={token.access_token}";
            var jsonStr = "{\"type\":\""+type+"\",\"offset\":" + offset + ",\"count\":" + count + "}";
            return NetworkHelper.Post(url, jsonStr);
        }

        public Stream GetStreamFromNetworkStream(Stream ns)
        {
            byte[] tmpBuffer = new byte[1024];
            List<byte> buffer = new List<byte>();
            var index = 0;
            while ((index = ns.Read(tmpBuffer, 0, tmpBuffer.Length)) != 0)
            {
                buffer.AddRange(tmpBuffer.ToList().Take(index));
            }
            return new MemoryStream(buffer.ToArray());
        }

        public HttpWebResponse GetForeverMaterial(string mediaId)
        {

            var token = _wechatTokenRepository.GetCurrentToken();
            var url = ConfigHelper.WetchatDomain +
                      $"/cgi-bin/material/get_material?access_token={token.access_token}";
            var jsonData = "{\"media_id\":\"" + mediaId + "\"}";
            return NetworkHelper.Post(url, jsonData);

        }


        public ForeverMaterialResult AddForverMaterial(string filePath, string type, Dictionary<string, string> dic = null)
        {
            var token = _wechatTokenRepository.GetCurrentToken();
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var ext = Path.GetExtension(filePath);
            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            {
                var url = ConfigHelper.WetchatDomain +
                      $"/cgi-bin/material/add_material?access_token={token.access_token}&type={type}";
                var response = NetworkHelper.PostWithMediaFile(url, fs, fileName, ext, "media", dic);
                var responseData = response.GetResponseData();
                var error = CheckError(responseData);
                if (error.HasError)
                {
                    return null;
                }
                return JsonConvert.DeserializeObject<ForeverMaterialResult>(responseData);
            }
        }
        public TemporaryMaterialResult AddTemporaryMaterial(string filePath, string type)
        {
            var token = _wechatTokenRepository.GetCurrentToken();
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var ext = Path.GetExtension(filePath);
            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            {
                var url = ConfigHelper.WetchatDomain +
                          $"/cgi-bin/media/upload?access_token={token.access_token}&type={type}";
                var response = NetworkHelper.PostWithMediaFile(url, fs, fileName, ext, "media", null);
                var responseData = response.GetResponseData();
                var error = CheckError(responseData);
                if (error.HasError)
                {
                    return null;
                }
                return JsonConvert.DeserializeObject<TemporaryMaterialResult>(responseData);
            }
        }

        #endregion

    }
}