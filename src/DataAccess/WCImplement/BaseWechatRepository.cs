﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;

namespace DataAccess.WCImplement
{
    public class BaseWechatRepository
    {
        protected Logger logger;

        public BaseWechatRepository()
        {
            logger = LogManager.GetLogger(GetType().FullName);
        }

        protected ErrorMessage CheckError(string json)
        {
            try
            {
                var error = JsonConvert.DeserializeObject<ErrorMessage>(json);
                return error;
            }
            catch (Exception e)
            {
                logger.Error(e);
                return new ErrorMessage();
            }
        }
    }
}
