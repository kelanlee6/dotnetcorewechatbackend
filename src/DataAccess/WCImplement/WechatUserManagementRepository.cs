﻿using System;
using System.Collections.Generic;
using System.Net;
using DataAccess.WCInterface;
using Database.Entities;
using Newtonsoft.Json;
using NLog;
using Utility;

namespace DataAccess.WCImplement
{
    class WechatUserManagementRepository : BaseWechatRepository, IWechatUserManagementRepository
    {
        public WechatUserTag CreateTag(string tagName)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            if (token==null)
            {
                //todo throw no token exception
                return null;
            }
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/create?access_token=" +token.access_token;
            var jsonStr = "{\"tag\":{\"name\":\"" + tagName + "\"}}";
            var response = NetworkHelper.Post(url,jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                logger.Info(dataStr);
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request create tag failed:{error.errcode} {error.errmsg}");
                    return null;
                }
                var result=JsonConvert.DeserializeObject<CreateTagResult>(dataStr);
                //todo：store the tag to DB
                return result.tag;
            }
            //todo throw exception
            return null;

        }

        public List<WechatUserTag> GetTags()
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/get?access_token=" + token.access_token;
            var response = NetworkHelper.Get(url);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request get tags failed:{error.errcode} {error.errmsg}");
                    return null;
                }
                var result = JsonConvert.DeserializeObject<GetTagsResult>(dataStr);
                return result.tags;
            }
            return null;
        }

        public bool EditTag(WechatUserTag tag)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            if (token == null)
            {
                //todo throw no token exception
                return false;
            }
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/update?access_token=" + token.access_token;
            var jsonStr = "{\"tag\":{\"id\":"+tag.id+",\"name\":\""+tag.name+"\"}}";
            var response = NetworkHelper.Post(url, jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                logger.Info(dataStr);
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request update tag failed:{error.errcode} {error.errmsg}");
                    return false;
                }
                //todo：update the tag to DB
                return true;
            }
            //todo throw exception
            return false;
        }

        public bool DeleteTag(int tagId)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            if (token == null)
            {
                //todo throw no token exception
                return false;
            }
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/delete?access_token=" + token.access_token;
            var jsonStr = "{\"tag\":{\"id\":"+tagId+"\"}}";
            var response = NetworkHelper.Post(url, jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                logger.Info(dataStr);
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request delete tag failed:{error.errcode} {error.errmsg}");
                    return false;
                }
                //todo：update the tag to DB
                return true;
            }
            //todo throw exception
            return false;
        }

        public List<string> GetTheUserWithTag(int tagId, ref string nextUserId, out int total)
        {
            string nextOpenId = nextUserId;
            total = 0;
            if (nextOpenId == null)
            {
                nextOpenId = string.Empty;
            }
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/user/tag/get?access_token=" + token.access_token + $"&next_openid={nextOpenId}";
            var jsonStr = "{\"tagid\":"+tagId+",\"next_openid\":\""+ nextOpenId + "\"}";
            var response = NetworkHelper.Post(url,jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request get tags failed:{error.errcode} {error.errmsg}");
                    return null;
                }
                var result = JsonConvert.DeserializeObject<GetUserInfoListResult>(dataStr);
                total = result.count;
                nextUserId = result.next_openid;
                return result.data.openid;
            }
            return null;
        }

        public bool TagTheUsers(int tagId, IList<string> userIds)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            if (token == null)
            {
                //todo throw no token exception
                return false;
            }
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/members/batchtagging?access_token=" + token.access_token;
            var jsonStr = "{\"openid_list\":[";
            foreach (var userId in userIds)
            {
                jsonStr += $"\"{userId}\",";
            }
            jsonStr = jsonStr.TrimEnd(',');
            jsonStr += "],\"tagid\":" + tagId + "}";
            var response = NetworkHelper.Post(url, jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                logger.Info(dataStr);
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request tag user failed:{error.errcode} {error.errmsg}");
                    return false;
                }
                return true;
            }
            return false;
        }

        public bool DiasbleTheTagForUsers(int tagId, IList<string> userIds)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            if (token == null)
            {
                //todo throw no token exception
                return false;
            }
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/members/batchuntagging?access_token=" + token.access_token;
            var jsonStr = "{\"openid_list\":[";
            foreach (var userId in userIds)
            {
                jsonStr += $"\"{userId}\",";
            }
            jsonStr = jsonStr.TrimEnd(',');
            jsonStr += "],\"tagid\":" + tagId + "}";
            var response = NetworkHelper.Post(url, jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                logger.Info(dataStr);
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request tag user failed:{error.errcode} {error.errmsg}");
                    return false;
                }
                return true;
            }
            return false;
        }

        public List<int> GetUserTags(string userId)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/getidlist?access_token=" + token.access_token;
            string jsonStr = "{\"openid\":\""+userId+"\"}";
            var response = NetworkHelper.Post(url, jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request get tags failed:{error.errcode} {error.errmsg}");
                    return null;
                }
                var result = JsonConvert.DeserializeObject<GetUserTagsResult>(dataStr);
                return result.tagid_list;
            }
            return null;
        }

        public bool SetRemarkForUser(string userId, string remark)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            if (token == null)
            {
                //todo throw no token exception
                return false;
            }
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/user/info/updateremark?access_token=" + token.access_token;
            var jsonStr = "{\"openid\":\""+userId+"\",\"remark\":\""+remark+"\"}";
            var response = NetworkHelper.Post(url, jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                logger.Info(dataStr);
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request tag user failed:{error.errcode} {error.errmsg}");
                    return false;
                }
                return true;
            }
            return false;
        }

        public WechatUser GetUser(string userId)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/user/info?access_token=" + token.access_token+ $"&openid={userId}&lang=zh_CN";
            var response = NetworkHelper.Get(url);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request get tags failed:{error.errcode} {error.errmsg}");
                    return null;
                }
                var result = JsonConvert.DeserializeObject<WechatUser>(dataStr);
                return result;
            }
            return null;
        }

        public List<WechatUser> GetUsers(List<string> userIds, string lang = "zh-CN")
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/user/info/batchget?access_token=" + token.access_token;
            var jsonStr = "{\"user_list\":[";
            foreach (var id in userIds)
            {
                jsonStr += "{\"openid\": \"" + id + "\",\"lang\": \"zh - CN\"}, ";
            }
            jsonStr = jsonStr.TrimEnd(',') + "]}";
            var response = NetworkHelper.Post(url,jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request get tags failed:{error.errcode} {error.errmsg}");
                    return null;
                }
                var result = JsonConvert.DeserializeObject<GetUsersResult>(dataStr);
                return result.user_info_list;
            }
            return null;
        }

        public List<string> GetUserListInfo(ref string nextId, out int total)
        {
            string nextOpenId = nextId;
            total = 0;
            if (nextOpenId==null)
            {
                nextOpenId = string.Empty;
            }
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/user/get?access_token=" + token.access_token + $"&next_openid={nextOpenId}";
            var response = NetworkHelper.Get(url);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request get tags failed:{error.errcode} {error.errmsg}");
                    return null;
                }
                var result = JsonConvert.DeserializeObject<GetUserInfoListResult>(dataStr);
                total = result.total;
                nextId = result.next_openid;
                return result.data.openid;
            }
            return null;
        }

        public List<string> GetBlackList(ref string nextUserId, out int total)
        {
            string nextOpenId = nextUserId;
            total = 0;
            if (nextOpenId == null)
            {
                nextOpenId = string.Empty;
            }
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/members/getblacklist?access_token=" + token.access_token + $"&next_openid={nextOpenId}";
            var jsonStr = "{\"begin_openid\":\""+nextUserId+"\"}";
            var response = NetworkHelper.Post(url,jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request get tags failed:{error.errcode} {error.errmsg}");
                    return null;
                }
                var result = JsonConvert.DeserializeObject<GetUserInfoListResult>(dataStr);
                total = result.total;
                nextUserId = result.next_openid;
                return result.data?.openid;
            }
            return null;
        }

        public bool MoveUsersToBlackList(List<string> userIds)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            if (token == null)
            {
                //todo throw no token exception
                return false;
            }
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/members/batchblacklist?access_token=" + token.access_token;
            var jsonStr = "{\"opened_list\":[";
            foreach (var userId in userIds)
            {
                jsonStr += $"\"{userId}\",";
            }
            jsonStr = jsonStr.TrimEnd(',');
            jsonStr += "]}";
            var response = NetworkHelper.Post(url, jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                logger.Info(dataStr);
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request tag user failed:{error.errcode} {error.errmsg}");
                    return false;
                }
                return true;
            }
            return false;
        }

        public bool MoveUsersOutOfBlackList(List<string> userIds)
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            if (token == null)
            {
                //todo throw no token exception
                return false;
            }
            var url = ConfigHelper.WetchatDomain + "/cgi-bin/tags/members/batchunblacklist?access_token=" + token.access_token;
            var jsonStr = "{\"opened_list\":[";
            foreach (var userId in userIds)
            {
                jsonStr += $"\"{userId}\",";
            }
            jsonStr = jsonStr.TrimEnd(',');
            jsonStr += "]}";
            var response = NetworkHelper.Post(url, jsonStr);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataStr = response.GetResponseData();
                logger.Info(dataStr);
                var error = CheckError(dataStr);
                if (error.HasError)
                {
                    logger.Error($"Request tag user failed:{error.errcode} {error.errmsg}");
                    return false;
                }
                return true;
            }
            return false;
        }
        
    }

}