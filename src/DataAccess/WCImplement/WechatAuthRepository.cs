﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using DataAccess.WCInterface;
using Database.Entities;
using Newtonsoft.Json;
using Utility;

namespace DataAccess.WCImplement
{
    class WechatAuthRepository : BaseWechatRepository, IWechatAuthRepository
    {
        public WechatToken GetAccessToken()
        {
            var url = ConfigHelper.WetchatDomain + $"/cgi-bin/token?grant_type=client_credential&appid={ConfigHelper.WechatAppId}&secret={ConfigHelper.WechatAppSecret}";
            var response = NetworkHelper.Get(url);
            var data = response.GetResponseData();
            if (response.StatusCode==HttpStatusCode.OK)
            {
                DateTime startDate=DateTime.Now;
                var token = JsonConvert.DeserializeObject<WechatToken>(data);
                if (token!=null)
                {
                    token.expired_date = startDate.AddMilliseconds(token.expires_in*1000);
                    return token;
                }
            }
            
            return null;
        }

        public List<string> GetWechatServerIPs()
        {
            var token = DataAccessManageFactory.CreateWechatTokenRepository().GetCurrentToken();
            var url = ConfigHelper.WetchatDomain + $"/cgi-bin/getcallbackip?access_token={token.access_token}";
            var response = NetworkHelper.Get(url);
            var data = response.GetResponseData();
            var error = CheckError(data);
            if (error.HasError)
            {
                return null;
            }
            var ipObj=JsonConvert.DeserializeObject<WechatServerIPs>(data);
            return ipObj.ip_list;
        }
    }
}