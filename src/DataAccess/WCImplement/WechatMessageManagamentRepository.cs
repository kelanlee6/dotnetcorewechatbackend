﻿using DataAccess.DBInterface;
using DataAccess.WCInterface;
using Newtonsoft.Json;
using Utility;

namespace DataAccess.WCImplement
{
    class WechatMessageManagamentRepository :BaseWechatRepository, IWechatMessageManagamentRepository
    {
        public IWechatTokenRepository WechatTokenRepository;

        public WechatMessageManagamentRepository()
        {
            WechatTokenRepository = DataAccessManageFactory.CreateWechatTokenRepository();
        }

        public MassMessageResponse SendMassImageTextMessageToTagUser(MassImageTextMessageToTagUser data)
        {
            return SendMassMessage(data,"tag");
        }


        public MassMessageResponse SendMassImageMessageToTagUser(MassImageMessageToTagUser data)
        {
            return SendMassMessage(data, "tag");
        }

        public MassMessageResponse SendMassTextMessageToTagUser(MassTextMessageToTagUser data)
        {
            return SendMassMessage(data, "tag");
        }

        public MassMessageResponse SendMassVideoMessageToTagUser(MassVideoMessageToTagUser data)
        {
            return SendMassMessage(data, "tag");
        }

        public MassMessageResponse SendMassVoiceMessageToTagUser(MassVoiceMessageToTagUser data)
        {
            return SendMassMessage(data, "tag");
        }

        public MassMessageResponse SendMassCardMessageToTagUser(MassCardMessageToTagUser data)
        {
            return SendMassMessage(data, "tag");
        }

        public MassMessageResponse SendMassImageTextMessageToOpenIds(MassImageTextMessageToOpenIds data)
        {
            return SendMassMessage(data, "openid");
        }

        public MassMessageResponse SendMassImageMessageToOpenIds(MassImageMessageToOpenIds data)
        {
            return SendMassMessage(data, "openid");
        }

        public MassMessageResponse SendMassTextMessageToOpenIds(MassTextMessageToOpenIds data)
        {
            return SendMassMessage(data, "openid");
        }

        public MassMessageResponse SendMassVideoMessageToOpenIds(MassVideoMessageToOpenIds data)
        {
            return SendMassMessage(data, "openid");
        }

        public MassMessageResponse SendMassVoiceMessageToOpenIds(MassVoiceMessageToOpenIds data)
        {
            return SendMassMessage(data, "openid");
        }

        public MassMessageResponse SendMassCardMessageToOpenIds(MassCardMessageToOpenIds data)
        {
            return SendMassMessage(data, "openid");
        }

        public MassMessageResponse SendPreviewMassImageTextMessageToOpenId(PreviewMassImageTextMessageToOpenId data)
        {
            return SendMassMessage(data, "preview");
        }

        public MassMessageResponse SendPreviewMassImageMessageToOpenId(PreviewMassImageMessageToOpenId data)
        {
            return SendMassMessage(data, "preview");
        }

        public MassMessageResponse SendPreviewMassTextMessageToOpenId(PreviewMassTextMessageToOpenId data)
        {
            return SendMassMessage(data, "preview");
        }

        public MassMessageResponse SendPreviewMassVideoMessageToOpenId(PreviewMassVideoMessageToOpenId data)
        {
            return SendMassMessage(data, "preview");
        }

        public MassMessageResponse SendPreviewMassVoiceMessageToOpenId(PreviewMassVoiceMessageToOpenId data)
        {
            return SendMassMessage(data, "preview");
        }

        public MassMessageResponse SendPreviewMassCardMessageToOpenId(PreviewMassCardMessageToOpenId data)
        {
            return SendMassMessage(data, "preview");
        }

        public bool DeleteMassMessage(int messageId)
        {
            var token = WechatTokenRepository.GetCurrentToken();
            string url = ConfigHelper.WetchatDomain + $"/cgi-bin/message/mass/delete?access_token={token.access_token}";
            var jsonData = "{\"msg_id\":"+messageId+"}";
            var response = NetworkHelper.Post(url, jsonData);
            var responseStr = response.GetResponseData();
            var error = CheckError(responseStr);
            return !error.HasError;
        }

        public MassMessageStatus GetMassMessageStatus(string msgId)
        {
            throw new System.NotImplementedException();
        }

        protected MassMessageResponse SendMassMessage(object data, string method)
        {
            var token = WechatTokenRepository.GetCurrentToken();
            string url= ConfigHelper.WetchatDomain;
            switch (method.ToLower())
            {
                case "tag":
                    url += $"/cgi-bin/message/mass/sendall?access_token={token.access_token}";
                    break;
                case "openid":
                    url  += $"/cgi-bin/message/mass/send?access_token={token.access_token}";
                    break;
                case "preview":
                    url+= $"/cgi-bin/message/mass/preview?access_token={token.access_token}";
                    break;
                default:
                    url += $"/cgi-bin/message/mass/sendall?access_token={token.access_token}";
                    break;
            }
            var jsonData = JsonConvert.SerializeObject(data);
            var response = NetworkHelper.Post(url, jsonData);
            var responseStr = response.GetResponseData();
            return JsonConvert.DeserializeObject<MassMessageResponse>(responseStr);
        }



    }
}