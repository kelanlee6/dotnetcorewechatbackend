﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Entities;

namespace DataAccess.DBInterface
{
    public interface IWechatTokenRepository: IRepository<WechatToken>
    {
        WechatToken GetCurrentToken();
        bool StoreToken(WechatToken token);
    }
}
