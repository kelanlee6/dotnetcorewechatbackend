﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.DBInterface;
using Database.Entities;

namespace DataAccess.DBInterface
{
    public interface IImageMessageRepository:IRepository<ImageMessage>
    {
        
    }
}
