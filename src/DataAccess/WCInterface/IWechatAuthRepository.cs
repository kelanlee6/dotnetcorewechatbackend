﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Entities;
using Newtonsoft.Json;

namespace DataAccess.WCInterface
{
    public interface IWechatAuthRepository
    {
        WechatToken GetAccessToken();

        List<string> GetWechatServerIPs();
    }
}
