﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.WCInterface
{
    public interface IWechatMessageManagamentRepository
    {
        //发送群消息给标记用户
        MassMessageResponse SendMassImageTextMessageToTagUser(MassImageTextMessageToTagUser data);
        MassMessageResponse SendMassImageMessageToTagUser(MassImageMessageToTagUser data);
        MassMessageResponse SendMassTextMessageToTagUser(MassTextMessageToTagUser data);
        MassMessageResponse SendMassVideoMessageToTagUser(MassVideoMessageToTagUser data);
        MassMessageResponse SendMassVoiceMessageToTagUser(MassVoiceMessageToTagUser data);
        MassMessageResponse SendMassCardMessageToTagUser(MassCardMessageToTagUser data);
        //发送群消息给用户列表中的用户
        MassMessageResponse SendMassImageTextMessageToOpenIds(MassImageTextMessageToOpenIds data);
        MassMessageResponse SendMassImageMessageToOpenIds(MassImageMessageToOpenIds data);
        MassMessageResponse SendMassTextMessageToOpenIds(MassTextMessageToOpenIds data);
        MassMessageResponse SendMassVideoMessageToOpenIds(MassVideoMessageToOpenIds data);
        MassMessageResponse SendMassVoiceMessageToOpenIds(MassVoiceMessageToOpenIds data);
        MassMessageResponse SendMassCardMessageToOpenIds(MassCardMessageToOpenIds data);

        //发送预览群消息给用户
        MassMessageResponse SendPreviewMassImageTextMessageToOpenId(PreviewMassImageTextMessageToOpenId data);
        MassMessageResponse SendPreviewMassImageMessageToOpenId(PreviewMassImageMessageToOpenId data);
        MassMessageResponse SendPreviewMassTextMessageToOpenId(PreviewMassTextMessageToOpenId data);
        MassMessageResponse SendPreviewMassVideoMessageToOpenId(PreviewMassVideoMessageToOpenId data);
        MassMessageResponse SendPreviewMassVoiceMessageToOpenId(PreviewMassVoiceMessageToOpenId data);
        MassMessageResponse SendPreviewMassCardMessageToOpenId(PreviewMassCardMessageToOpenId data);
        //删除群发
        bool DeleteMassMessage(int messageId);
        //群发消息状态
        MassMessageStatus GetMassMessageStatus(string msgId);
    }
}
