﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.WCInterface
{
    public interface IWechatMaterialManagementRepository
    {
        //添加和获取临时素材
        TemporaryMaterialResult AddTemporaryVideoMaterial(string filePath);
        TemporaryMaterialResult AddTemporaryVoiceMaterial(string filePath);
        TemporaryMaterialResult AddTemporaryImageMaterial(string filePath);
        TemporaryMaterialResult AddTemporaryThumbMaterial(string filePath);
        Stream GetMaterial(string mediaId,out string fileName, out int length);

        //添加永久素材
        ForeverMaterialResult AddForverVideoMaterial(string filePath);
        ForeverMaterialResult AddForverVoiceMaterial(string filePath);
        ForeverMaterialResult AddForverImageMaterial(string filePath);
        //ForeverMaterialResult AddForverThumbMaterial(string filePath);
        URLResult AddImageForImageTextMaterial(string filePath);
        ForeverMaterialResult AddForverImageTextMaterial(AddImageTextMaterialRequest data);
        
        //获取永久素材
        ForeverVideoMaterialResult GetForeverVideoMaterial(string mediaId);
        Stream GetForeverVoiceMaterial(string mediaId);
        Stream GetForeverImageMaterial(string mediaId);
        Stream GetForeverThumbMaterial(string mediaId);
        ImageTextMaterialResult GetForeverImageTextMaterial(string mediaId);

        //删除永久素材
        bool DeleteForeverMaterial(string mediaId);
        //修改永久图文素材
        bool UpdateImageTextMaterial(UpdateImageTextMaterialRequest data);

        MaterialCount GetMaterialCount();

        //获取素材列表

        ForeverImageTextMaterialList GetImagrForeverImageTextMaterialList(int offset=0,int count=20);

        ForeverMaterialList GetForeverImageMaterialList(int offset = 0, int count = 20);
        ForeverMaterialList GetForeverVideoMaterialList(int offset = 0, int count = 20);
        ForeverMaterialList GetForeverVoiceMaterialList(int offset = 0, int count = 20);
        //ForeverMaterialList GetForeverThumbMaterialList(int offset = 0, int count = 20);
    }
}
