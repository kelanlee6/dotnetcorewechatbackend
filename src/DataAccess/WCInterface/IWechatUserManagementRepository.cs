﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.WCInterface
{
    public interface IWechatUserManagementRepository
    {
        //用户标签管理
        WechatUserTag CreateTag(string tagName);//done
        List<WechatUserTag> GetTags();//done
        bool EditTag(WechatUserTag tag);//done
        bool DeleteTag(int tagId);//done
        //
        List<string> GetTheUserWithTag(int tagId,ref string nextUserId, out int total);//done
        bool TagTheUsers(int tagId, IList<string> userIds);//done
        bool DiasbleTheTagForUsers(int tagId, IList<string> userIds);//done
        List<int> GetUserTags(string userId);//done
        //用户备注名
        bool SetRemarkForUser(string userId, string remark);//done
        //用户信息获取
        WechatUser GetUser(string userId);//done
        List<WechatUser> GetUsers(List<string> userIds, string lang = "zh-CN");//done
        //获取用户列表 
        //GetUserListInfo每次最多获取10000条用户id，如果用户数量超过10000，第10000条的userId会out出来
        //如果从0开始获取用户数据，传入nextId为null或者string.Empty,如果需要从某个UserId开始获取，把该Id传入nextId
        List<string> GetUserListInfo(ref string nextId,out int total);//done
        
        //黑名单管理
        //GetBlackList限制与GetUserListInfo一样
        List<string> GetBlackList(ref string nextUserId,out int total);
        //最多只能20个
        bool MoveUsersToBlackList(List<string> userIds);
        //限制20个
        bool MoveUsersOutOfBlackList(List<string> userIds);
    }
}
