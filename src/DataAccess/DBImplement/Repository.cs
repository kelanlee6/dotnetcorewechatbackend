﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.DBInterface;
using Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using NLog;

namespace DataAccess.DBImplement
{
    public class Repository<TObject> :IRepository<TObject>
        where TObject : class
    {
        protected Logger Log;
        protected WechatDbContext Context = null;
        private bool shareContext = false;

        public Repository()
        {
            Context = new WechatDbContextFactory().Create(new DbContextFactoryOptions());
            Log= LogManager.GetLogger(GetType().FullName);
        }

        public Repository(WechatDbContext context)
        {
            Context = context;
            shareContext = true;
        }

        protected DbSet<TObject> DbSet
        {
            get
            {
                return Context.Set<TObject>();
            }
        }

        public void Dispose()
        {
            if (shareContext && (Context != null))
                Context.Dispose();
        }

        public virtual IQueryable<TObject> All()
        {
            return DbSet.AsQueryable();
        }

        public virtual IQueryable<TObject> Filter(Expression<Func<TObject, bool>> predicate)
        {
            Log.Info("Filter: " + predicate);
            return DbSet.Where(predicate).AsQueryable<TObject>();
        }

        public IQueryable<TObject> Filter<Key>(Expression<Func<TObject, bool>> filter, out int total, int index = 0, int size = 50)
        {
            int skipCount = index * size;
            var _resetSet = filter != null ? DbSet.Where(filter).AsQueryable() :
                DbSet.AsQueryable();
            _resetSet = skipCount == 0 ? _resetSet.Take(size) :
                _resetSet.Skip(skipCount).Take(size);
            total = _resetSet.Count();
            return _resetSet.AsQueryable();
        }

        public virtual IQueryable<TObject> Filter(Expression<Func<TObject, bool>> filter, out int total, int index = 0, int size = 50)
        {
            int skipCount = index * size;
            var _resetSet = filter != null ? DbSet.Where(filter).AsQueryable() :
                DbSet.AsQueryable();
            _resetSet = skipCount == 0 ? _resetSet.Take(size) :
                _resetSet.Skip(skipCount).Take(size);
            total = _resetSet.Count();
            return _resetSet.AsQueryable();
        }

        public bool Contains(Expression<Func<TObject, bool>> predicate)
        {
            Log.Info("Contains: " + predicate);
            return DbSet.Count(predicate) > 0;
        }

        //public virtual TObject Find(params object[] keys)
        //{
        //    Log.Info("Find: " + keys);
        //    var result=DbSet.(keys);
        //}

        public virtual TObject Find(Expression<Func<TObject, bool>> predicate)
        {
            Log.Info("Find: " + predicate);
            return DbSet.FirstOrDefault(predicate);
        }

        public virtual TObject Create(TObject TObject)
        {
            EntityEntry<TObject> newEntry = DbSet.Add(TObject);
            if (!shareContext)
                Context.SaveChanges();
            return newEntry.Entity;
        }
        public virtual List<TObject> Create(List<TObject> objects)
        {
            List<TObject> result = objects.Select(obj => DbSet.Add(obj).Entity).ToList();
            if (!shareContext)
                Context.SaveChanges();
            return result;
        }
        void IRepository<TObject>.Delete(TObject t)
        {
            DbSet.Remove(t);
            if (!shareContext)
                Context.SaveChanges();
        }

        public virtual int Count
        {
            get
            {
                return DbSet.Count();
            }
        }
        
        public virtual int Delete(TObject TObject)
        {
            DbSet.Remove(TObject);
            if (!shareContext)
                return Context.SaveChanges();
            return 0;
        }

        public virtual int Update(TObject TObject)
        {
            var entry = Context.Entry(TObject);
            DbSet.Attach(TObject);
            entry.State = EntityState.Modified;
            if (!shareContext)
                return Context.SaveChanges();
            return 0;
        }

        public virtual int Delete(Expression<Func<TObject, bool>> predicate)
        {
            Log.Info("Delete: " + predicate);
            var objects = Filter(predicate);
            foreach (var obj in objects)
                DbSet.Remove(obj);
            if (!shareContext)
                return Context.SaveChanges();
            return 0;
        }
    }
}
