﻿using System;
using DataAccess.DBInterface;
using Database.Entities;
using Utility;

namespace DataAccess.DBImplement
{
    class WechatTokenRepository:Repository<WechatToken>,IWechatTokenRepository
    {
        public WechatToken GetCurrentToken()
        {
            var token = Find(m => m.AppId==ConfigHelper.WechatAppId);
            
            return token;
        }

        public bool StoreToken(WechatToken token)
        {
            var currentToken = Find(m => m.AppId == token.AppId);
            bool isUpdate = currentToken != null;
            if (isUpdate)
            {
                currentToken.access_token = token.access_token;
                currentToken.expires_in = token.expires_in;
                currentToken.expired_date = token.expired_date;
                currentToken.Secret = token.Secret;
                Update(currentToken);
            }
            else
            {
                Create(token);
            }
            return true;
        }
    }
}
