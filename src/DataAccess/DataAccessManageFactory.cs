﻿using DataAccess.DBImplement;
using DataAccess.DBInterface;
using DataAccess.WCImplement;
using DataAccess.WCInterface;

namespace DataAccess
{
    public static class DataAccessManageFactory
    {
        public static IWechatUserRepository CreateWechatUserRepository()
        {
            return new WechatUserRepository();
        }
        public static IMenuEventRepository CreateMenuEventRepository()
        {
            return new MenuEventRepository();
        }
        public static IImageMessageRepository CreateImageMessageRepository()
        {
            return new ImageMessageRepository();
        }
        public static ILinkMessageRepository CreateLinkMessageRepository()
        {
            return new LinkMessageRepository();
        }

        public static ILocationEventRepository CreateLocationEventRepository()
        {
            return new LocationEventRepository();
        }

        public static ILocationMessageRepository CreateLocationMessageRepository()
        {
            return new LocationMessageRepository();
        }

        public static IShortVideoMessageRepository CreateShortVideoMessageRepository()
        {
            return new ShortVideoMessageRepository();
        }

        public static ISubscribeEventRepository CreateSubscribeEventRepository()
        {
            return new SubscribeEventRepository();
        }

        public static ITextMessageRepository CreateTextMessageRepository()
        {
            return new TextMessageRepository();
        }

        public static IVideoMessageRepository CreateVideoMessageRepository()
        {
            return new VideoMessageRepository();
        }

        public static IVoiceMessageRepository CreateVoiceMessageRepository()
        {
            return new VoiceMessageRepository();
        }

        public static IWechatTokenRepository CreateWechatTokenRepository()
        {
            return new WechatTokenRepository();
        }
    }
}
