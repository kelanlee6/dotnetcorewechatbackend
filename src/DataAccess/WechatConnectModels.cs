﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Entities;

namespace DataAccess
{
    public class ErrorMessage
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }

        public bool HasError => errcode != 0;
    }

    public class MassMessageResponse
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public int msg_id { get; set; }
        public int msg_data_id { get; set; }
        public bool HasError => errcode != 0;
    }

    public class MassMessageStatus
    {
        public string msg_id { get; set; }
        public string msg_status { get; set; }
    }

    #region UserManagement

    public class CreateTagResult
    {
        public WechatUserTag tag { get; set; }
    }

    public class GetTagsResult
    {
        public List<WechatUserTag> tags { get; set; }
    }
    public class GetUsersResult
    {
        public List<WechatUser> user_info_list { get; set; }
    }

    public class GetUserInfoListResult
    {
        public int total { get; set; }
        public int count { get; set; }
        public OpenIdList data { get; set; }
        public string next_openid { get; set; }

    }

    public class OpenIdList
    {
        public List<string> openid { get; set; }

    }

    public class GetUserTagsResult
    {
        public List<int> tagid_list { get; set; }

    }

    #endregion

    #region Material
    public class TemporaryMaterialResult
    {
        public string type { get; set; }
        public string media_id { get; set; }
        public int created_at { get; set; }
    }
    public class ForeverMaterialResult
    {
        public string media_id { get; set; }
        public string url { get; set; }
    }
    public class URLResult
    {
        public string url { get; set; }
    }
    public class VideoMaterialResult
    {
        public string video_url { get; set; }
    }

    public class ImageTextMaterialRequestData
    {
        public string title { get; set; }
        public string thumb_media_id { get; set; }
        public string author { get; set; }
        public string digest { get; set; }
        public int show_cover_pic { get; set; }
        public string content { get; set; }
        public string content_source_url { get; set; }
    }
    public class AddImageTextMaterialRequest
    {
        public List<ImageTextMaterialRequestData> articles { get; set; }
    }

    public class ImageTextMaterialResult
    {
        public List<ImageTextMaterialResultData> news_item { get; set; }
        public int create_time { get; set; }
        public int update_time { get; set; }
    }

    public class ImageTextMaterialResultData
    {
        public string title { set; get; }
        public string thumb_media_id { set; get; }
        public string show_cover_pic { set; get; }
        public string author { set; get; }
        public string digest { set; get; }
        public string content { set; get; }
        public string url { set; get; }
        public string content_source_url { set; get; }
        public string thumb_url { set; get; }

    }

    public class ForeverVideoMaterialResult
    {
        public string title { get; set; }
        public string description { get; set; }
        public string down_url { get; set; }
    }

    public class UpdateImageTextMaterialRequest
    {
        public string media_id { get; set; }
        public int index { get; set; }
        public ImageTextMaterialResultData articles { get; set; }
    }

    public class MaterialCount
    {
        public int voice_count { get; set; }
        public int video_count { get; set; }
        public int image_count { get; set; }
        public int news_count { get; set; }

    }

    public class  ForeverImageTextMaterialList
    {
        public int total_count { get; set; }
        public int item_count { get; set; }
        public List<ForeverImageTextMaterialItem> item { get; set; }
    }
    public class ForeverMaterialList
    {
        public int total_count { get; set; }
        public int item_count { get; set; }
        public List<ForeverMaterialItem> item { get; set; }
    }

    public class ForeverImageTextMaterialItem
    {
        public string media_id { get; set; }
        public ImageTextMaterialResult content { get; set; }
        public int update_time { get; set; }
    }

    

    public class ForeverMaterialItem
    {
        public string media_id { get; set; }
        public string name { get; set; }
        public int update_time { get; set; }
        public string url { get; set; }
    }

    #endregion


    public class WechatServerIPs
    {
        public List<string> ip_list { get; set; }
    }

    #region MassMessageToTagUser

    public class TagMessageFilter
    {
        public bool is_to_all { get; set; }
        public int tag_id { get; set; }
    }

    public class MassMessageMedia
    {
        public string media_id { get; set; }
    }
    public class MassMessageText
    {
        public string content { get; set; }
    }
    public class MassMessageCard
    {
        public string card_id { get; set; }
    }
    public class MassMessageVideo
    {
        public string media_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }

    public class MassImageMessageToTagUser
    {
        public TagMessageFilter filter { get; set; }
        public MassMessageMedia image { get; set; }
        public string msgtype => "image";
    }
    public class MassTextMessageToTagUser
    {
        public TagMessageFilter filter { get; set; }
        public MassMessageText text { get; set; }
        public string msgtype => "text";
    }
    public class MassVoiceMessageToTagUser
    {
        public TagMessageFilter filter { get; set; }
        public MassMessageMedia voice { get; set; }
        public string msgtype => "voice";
    }
    public class MassVideoMessageToTagUser
    {
        public TagMessageFilter filter { get; set; }
        public MassMessageMedia mpvideo { get; set; }
        public string msgtype => "mpvideo";
    }
    public class MassImageTextMessageToTagUser
    {
        public TagMessageFilter filter { get; set; }
        public MassMessageMedia mpnews { get; set; }
        public string msgtype => "mpnews";
    }

    public class MassCardMessageToTagUser
    {
        public TagMessageFilter filter { get; set; }
        public MassMessageCard wxcard { get; set; }
        public string msgtype => "wxcard";
    }

    #endregion

    #region MassMessageToOpenIdList


    public class MassImageMessageToOpenIds
    {
        public List<string> touser { get; set; }
        public MassMessageMedia image { get; set; }
        public string msgtype => "image";
    }
    public class MassTextMessageToOpenIds
    {
        public List<string> touser { get; set; }
        public MassMessageText text { get; set; }
        public string msgtype => "text";
    }
    public class MassVoiceMessageToOpenIds
    {
        public List<string> touser { get; set; }
        public MassMessageMedia voice { get; set; }
        public string msgtype => "voice";
    }
    public class MassVideoMessageToOpenIds
    {
        public List<string> touser { get; set; }
        public MassMessageVideo mpvideo { get; set; }
        public string msgtype => "mpvideo";
    }
    public class MassImageTextMessageToOpenIds
    {
        public List<string> touser { get; set; }
        public MassMessageMedia mpnews { get; set; }
        public string msgtype => "mpnews";
    }

    public class MassCardMessageToOpenIds
    {
        public List<string> touser { get; set; }
        public MassMessageCard wxcard { get; set; }
        public string msgtype => "wxcard";
    }

    #endregion


    #region PreviewMassMessageToOpenId


    public class PreviewMassImageMessageToOpenId
    {
        public string touser { get; set; }
        public MassMessageMedia image { get; set; }
        public string msgtype => "image";
    }
    public class PreviewMassTextMessageToOpenId
    {
        public string touser { get; set; }
        public MassMessageText text { get; set; }
        public string msgtype => "text";
    }
    public class PreviewMassVoiceMessageToOpenId
    {
        public string touser { get; set; }
        public MassMessageMedia voice { get; set; }
        public string msgtype => "voice";
    }
    public class PreviewMassVideoMessageToOpenId
    {
        public string touser { get; set; }
        public MassMessageVideo mpvideo { get; set; }
        public string msgtype => "mpvideo";
    }
    public class PreviewMassImageTextMessageToOpenId
    {
        public string touser { get; set; }
        public MassMessageMedia mpnews { get; set; }
        public string msgtype => "mpnews";
    }

    public class PreviewMassCardMessageToOpenId
    {
        public string touser { get; set; }
        public MassMessageCard wxcard { get; set; }
        public string msgtype => "wxcard";
    }

    #endregion
}
