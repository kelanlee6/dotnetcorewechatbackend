﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Context.Implement;
using Context.Interface;

namespace Context
{
    public static class ContextManageFactory
    {
        public static IMessageContext CreateMessageContext()
        {
            return new MessageContext();
        }

        public static IUserManagementContext CreateUserManagementContext()
        {
            return new UserManagementContext();
        }
        public static IWechatAuthenticationContext CreateWechatAuthenticationContext()
        {
            return new WechatAuthenticationContext();
        }
    }
}
