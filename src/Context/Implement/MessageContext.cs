﻿using System;
using Context.Interface;
using DataAccess;
using DataAccess.DBInterface;
using Database.Entities;
using Models;
using Models.Enum;

namespace Context.Implement
{
    class MessageContext : ContextBase, IMessageContext
    {
        public MessageContext()
        {
        }

        public bool StoreMessage(MessageModel msg)
        {
            switch (msg.MsgType)
            {
                case MessageType.Text:
                    return StoreTextMessage(msg);
                case MessageType.Image:
                    return StoreImageMessage(msg);
                case MessageType.Voice:
                    return StoreVoiceMessage(msg);
                case MessageType.Video:
                    return StoreVideoMessage(msg);
                case MessageType.shortvideo:
                    return StoreShortVideoMessage(msg);
                case MessageType.Location:
                    return StoreLocationMessage(msg);
                case MessageType.Link:
                    return StoreLinkMessage(msg);
                case MessageType.Event:
                    return StoreEvent(msg);
                case MessageType.Music:
                    break;
                case MessageType.News:
                    break;
                default:
                    return false;
            }
            return false;
        }

        private bool StoreTextMessage(MessageModel msg)
        {
            try
            {
                var entity = new TextMessage();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateTextMessageRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }
        private bool StoreImageMessage(MessageModel msg)
        {
            try
            {
                var entity = new ImageMessage();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateImageMessageRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }
        private bool StoreLinkMessage(MessageModel msg)
        {
            try
            {
                var entity = new LinkMessage();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateLinkMessageRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }
        private bool StoreLocationMessage(MessageModel msg)
        {
            try
            {
                var entity = new LocationMessage();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateLocationMessageRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }
        private bool StoreVideoMessage(MessageModel msg)
        {
            try
            {
                var entity = new VideoMessage();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateVideoMessageRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }
        private bool StoreVoiceMessage(MessageModel msg)
        {
            try
            {
                var entity = new VoiceMessage();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateVoiceMessageRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }
        private bool StoreShortVideoMessage(MessageModel msg)
        {
            try
            {
                var entity = new ShortVideoMessage();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateShortVideoMessageRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }
        private bool StoreEvent(MessageModel msg)
        {
            switch (msg.Event)
            {
                case EventType.subscribe:
                    return StoreSubscribeEvent(msg);
                case EventType.unsubscribe:
                    return StoreSubscribeEvent(msg);
                case EventType.LOCATION:
                    return StoreLocationEvent(msg);
                case EventType.CLICK:
                    return StoreMenuEvent(msg);
                case EventType.VIEW:
                    return StoreMenuEvent(msg);
                case EventType.others:
                default:
                    return false;
            }
        }

        private bool StoreSubscribeEvent(MessageModel msg)
        {
            try
            {
                var entity = new SubscribeEvent();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateSubscribeEventRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }
        private bool StoreLocationEvent(MessageModel msg)
        {
            try
            {
                var entity = new LocationEvent();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateLocationEventRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }
        private bool StoreMenuEvent(MessageModel msg)
        {
            try
            {
                var entity = new MenuEvent();
                entity.Transfer(msg);
                DataAccessManageFactory.CreateMenuEventRepository().Create(entity);
                return true;
            }
            catch (Exception e)
            {
                Log.Error($"{e}");
                return false;
            }
        }

    }
}