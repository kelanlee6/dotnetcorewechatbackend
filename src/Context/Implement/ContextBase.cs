﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using NLog;

namespace Context.Implement
{
    public class ContextBase
    {
        public Logger Log { get; private set; }

        public ContextBase()
        {
            Log= LogManager.GetLogger(GetType().FullName);
        }
    }
}
