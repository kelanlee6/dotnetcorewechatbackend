﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Context.Interface;
using DataAccess;
using DataAccess.DBInterface;
using Database.Entities;
using Senparc.Weixin;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.User;

namespace Context.Implement
{
    public class UserManagementContext:ContextBase ,IUserManagementContext
    {
        //private IWechatUserManagementRepository _wechatUserManagementRepository;
        private readonly IWechatAuthenticationContext _wechatAuthenticationContext;
        private readonly IWechatUserRepository _wechatUserRepository;
        public UserManagementContext()
        {
            //_wechatUserManagementRepository = WechatInfoAccessManageFactory.CreateWechatUserManagementRepository();
            _wechatUserRepository = DataAccessManageFactory.CreateWechatUserRepository();
            _wechatAuthenticationContext = ContextManageFactory.CreateWechatAuthenticationContext();
        }

        public bool UpdateAllWechatUser()
        {
            //GetAllUserIdList
            List<string> userIds=new List<string>();
            List<WechatUser> users=new List<WechatUser>();
            string nextId=string.Empty;
            
            var result = UserApi.Get(_wechatAuthenticationContext.GetWechatToken(), "");
            int total=result.total;
            userIds.AddRange(result.data.openid);
            if (total>10000)
            {
                int counter1 = total/10000;
                for (int i = 0; i < counter1; i++)
                {
                    result = UserApi.Get(_wechatAuthenticationContext.GetWechatToken(), result.next_openid);
                    userIds.AddRange(result.data.openid);
                }
            }
            //GetAllUserData
            int counter2 = userIds.Count/100 + 1;
            for (int i = 0; i < counter2; i++)
            {
                int num = (i + 1)*100 < userIds.Count ? 100 : userIds.Count - i*100;
                var batchUserList =
                    userIds.GetRange(i*100, num)
                        .Select(m => new BatchGetUserInfoData() {openid = m, lang = Language.zh_CN})
                        .ToList();
                var usersInfo=UserApi.BatchGetUserInfo(_wechatAuthenticationContext.GetWechatToken(), batchUserList).user_info_list;
                ;
                users.AddRange(usersInfo.Select(m =>
                {
                    var user = new WechatUser();
                    user.Transfer(m);
                    return user;
                }));
            }
            //storeToDatabase
            foreach (var user in users)
            {
                SaveOrUpdateWechatUser(user);
            }
            return true;
        }

        public bool SaveOrUpdateWechatUser(string userId)
        {
            var result = UserApi.Info(_wechatAuthenticationContext.GetWechatToken(), userId);
            var user=new WechatUser();
            user.Transfer(result);
            return SaveOrUpdateWechatUser(user);
        }

        public bool SaveOrUpdateWechatUser(WechatUser user)
        {
            var userInDB = _wechatUserRepository.Find(m => m.openid == user.openid);
            if (userInDB == null)
            {
                _wechatUserRepository.Create(user);
            }
            else
            {
                userInDB.subscribe = user.subscribe;
                userInDB.nickname = user.nickname;
                userInDB.sex = user.sex;
                userInDB.city = user.city;
                userInDB.country = user.country;
                userInDB.province = user.province;
                userInDB.language = user.language;
                userInDB.headimgurl = user.headimgurl;
                userInDB.subscribe_time = user.subscribe_time;
                userInDB.unionid = user.unionid;
                userInDB.remark = user.remark;
                userInDB.groupid = user.groupid;
                userInDB.tagid_listStr = user.tagid_listStr;
                _wechatUserRepository.Update(userInDB);
            }

            return true;
        }
    }
}
