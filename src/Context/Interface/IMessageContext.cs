﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;

namespace Context.Interface
{
    public interface IMessageContext
    {
        bool StoreMessage(MessageModel msg);
    }
}
