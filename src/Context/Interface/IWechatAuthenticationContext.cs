﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Database.Entities;
using Models;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.CommonAPIs;
using Utility;

namespace Context.Interface
{
    public interface IWechatAuthenticationContext
    {
        string GetWechatToken();
    }

    class WechatAuthenticationContext : IWechatAuthenticationContext
    {
        public string GetWechatToken()
        {
            var repo= DataAccessManageFactory.CreateWechatTokenRepository();
            var token = repo.GetCurrentToken();
            if (token==null||token.expired_date<DateTime.Now)
            {
                var startTime = DateTime.Now;
                var newToken=CommonApi.GetToken(ConfigHelper.WechatAppId, ConfigHelper.WechatAppSecret);
                token=new WechatToken()
                {
                    AppId = ConfigHelper.WechatAppId,
                    Secret = ConfigHelper.WechatAppSecret,
                    access_token = newToken.access_token,
                    expires_in = newToken.expires_in,
                    expired_date = startTime.AddMilliseconds(newToken.expires_in * 1000)
                };
                repo.StoreToken(token);
            }
            return token.access_token;
        }
    }
}

