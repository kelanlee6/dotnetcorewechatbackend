﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Entities;

namespace Context.Interface
{
    public interface IUserManagementContext
    {
        bool UpdateAllWechatUser();
        bool SaveOrUpdateWechatUser(string userId);
    }
}
