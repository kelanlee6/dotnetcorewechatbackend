﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;
namespace Services
{
    public interface IMessageService
    {
        MessageModel AnalysisMessageFromXmlStr(string xmlStr);
        bool StoreMessage(MessageModel msg);
    }
}
