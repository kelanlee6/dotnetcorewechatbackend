﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Services.Implement;

namespace Services
{
    public static class ServiceManagerFactory
    {
        public static IMessageService CreateMessageService()
        {
            return new MessageService();
        }
    }
}
