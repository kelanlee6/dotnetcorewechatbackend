﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NLog;

namespace Services
{
    public class ServiceBase
    {
        public ServiceBase()
        {
            Logger = LogManager.GetLogger(GetType().FullName);
        }
        

        protected Logger Logger { get; private set; }
    }
}
