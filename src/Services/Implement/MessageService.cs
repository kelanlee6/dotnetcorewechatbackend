﻿using System;
using System.Xml;
using Context;
using Microsoft.Extensions.Logging;
using Models;
using LogLevel = NLog.LogLevel;

namespace Services.Implement
{
    public class MessageService : ServiceBase,IMessageService
    {
        public MessageService()
        {
            
        }

        public MessageModel AnalysisMessageFromXmlStr(string xmlStr)
        {
            Logger.Log(LogLevel.Debug,$"recevice xml :{xmlStr}");
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlStr);
            var msg=new MessageModel(xmlDocument);
            return msg;
        }

        public bool StoreMessage(MessageModel msg)
        {
            Logger.Log(LogLevel.Debug, $"store message :{msg}");
            return ContextManageFactory.CreateMessageContext().StoreMessage(msg);
        }
    }
}