﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using NLog;

namespace Utility
{
    public static class ConfigHelper
    {
        private static readonly IConfigurationRoot Config;

        static ConfigHelper()
        {
            if (Config == null)
            {
                var builder = new ConfigurationBuilder();
                builder.SetBasePath(Directory.GetCurrentDirectory());
                builder.AddJsonFile("appsettings.json");
                var connectionStringConfig = builder.Build();

                // chain calls together as a fluent API
                Config = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

                //var builder = new ConfigurationBuilder();
                //builder.AddInMemoryCollection();
                //config = builder.Build();
            }

        }

        private static T GetValueFromConfig<T>(string settingString, T defaultValue = default(T),
            bool throwException = true)
        {
            try
            {
                return Config.GetValue<T>(settingString, defaultValue);
            }
            catch (Exception e)
            {
                LogManager.GetLogger("ConfigHelper").Error(e);
                if (throwException)
                    throw e;
                return defaultValue;
            }
            
        }

        public static string WechatToken => GetValueFromConfig("WechatToken", "123456");
        public static string WechatAppId => GetValueFromConfig("AppId", "wx8de8692fa057d9ee");
        public static string WechatAppSecret => GetValueFromConfig("AppSecret", "ed7b69ee712773139757c02d8b503313");

        public static string SqliteDatabasePath
            => GetValueFromConfig("SqliteDatabasePath", "./wechat.db",false);
        public static bool RepeatReply => GetValueFromConfig("RepeatReply", false);
        public static string WetchatDomain => GetValueFromConfig("WetchatDomain", "http://api.weixin.qq.com",false);

        public static string DataBaseType => GetValueFromConfig("DataBaseType", "sqlite", false);
        public static string SqlServerConnectionString => GetValueFromConfig("SqlServerConnectionString", "", false);

    }
}
