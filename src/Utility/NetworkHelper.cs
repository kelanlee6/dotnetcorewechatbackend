﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using NLog.LayoutRenderers.Wrappers;

namespace Utility
{
    public static class NetworkHelper
    {

        public static HttpWebResponse Get(string url)
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
            request.Method = "GET";
            //responseTask.Start();
            //Task.WaitAll(responseTask);
            return (HttpWebResponse)request.GetResponseAsync().Result;


        }


        public static HttpWebResponse Post(string url, string data, string contextType = "application/json")
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = contextType;
            using (var stream = request.GetRequestStreamAsync().Result)
            {
                var bytes = Encoding.UTF8.GetBytes(data);
                stream.Write(bytes, 0, bytes.Length);
            }
            return (HttpWebResponse) request.GetResponseAsync().Result;
        }

        public static HttpWebResponse PostWithMediaFile(string url, FileStream stream,string fileName,string fileExt, string fileType,Dictionary<string,string> stringDict)
        {
            var memStream = new MemoryStream();
            //set the request
            var webRequest = WebRequest.Create(url);
            var boundary = "---------------" +Guid.NewGuid().ToString().Trim('-');
            var beginBoundary = Encoding.ASCII.GetBytes("--" + boundary + "\r\n");
            var endBoundary = Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            webRequest.Method = "POST";
            webRequest.Headers["Connection"] = "Keep-Alive";
            webRequest.Headers["Charset"] = "UTF-8";
            webRequest.ContentType = "multipart/form-data; boundary=" + boundary;
            //Set the Payload
            const string filePartHeader =
                "Content-Disposition: form-data; name=\"{0}\";filelength=\"{1}\"; filename=\"{2}\"\r\n" +
                "Content-Type: :application/octet-stream\r\n\r\n";
            var header = string.Format(filePartHeader, fileType,stream.Length, $"{fileName}{fileExt}");
            var headerbytes = Encoding.UTF8.GetBytes(header);

            memStream.Write(beginBoundary, 0, beginBoundary.Length);
            memStream.Write(headerbytes, 0, headerbytes.Length);
            var buffer = new byte[1024];
            int bytesRead; // =0  
            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) != 0)
            {
                memStream.Write(buffer, 0, bytesRead);
            }
            // 写入字符串的Key  
            if (stringDict!=null)
            {
                var stringKeyHeader = "\r\n--" + boundary +
                                   "\r\nContent-Disposition: form-data; name=\"{0}\"" +
                                   "\r\n\r\n{1}\r\n";

                foreach (byte[] formitembytes in from string key in stringDict.Keys
                                                 select string.Format(stringKeyHeader, key, stringDict[key])
                                                     into formitem
                                                 select Encoding.UTF8.GetBytes(formitem))
                {
                    memStream.Write(formitembytes, 0, formitembytes.Length);
                }
            }
            
            memStream.Write(endBoundary, 0, endBoundary.Length);
            var requestStream = webRequest.GetRequestStreamAsync().Result;

            memStream.Position = 0;
            var tempBuffer = new byte[memStream.Length];
            memStream.Read(tempBuffer, 0, tempBuffer.Length);
            requestStream.Write(tempBuffer, 0, tempBuffer.Length);
            //Get the response
            var httpWebResponse = (HttpWebResponse) webRequest.GetResponseAsync().Result;
            return httpWebResponse;
        }
    }
}
