﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Utility
{
    public static class Extensions
    {
        public static string GetResponseData(this HttpWebResponse obj)
        {
            var sr=new StreamReader(obj.GetResponseStream());
            return sr.ReadToEnd();
        }

        public static string GetResponseData(this HttpWebResponse obj,Stream stream)
        {
            var sr = new StreamReader(stream);
            return sr.ReadToEnd();
        }
    }
}
