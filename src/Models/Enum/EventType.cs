﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Enum
{
    public enum EventType
    {
        subscribe = 1,
        unsubscribe = 2,
        LOCATION = 3,
        CLICK = 4,
        VIEW = 5,
        others=0
    }
}
