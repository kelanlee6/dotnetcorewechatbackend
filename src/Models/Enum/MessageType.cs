﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Enum
{
    public enum MessageType
    {
        Text = 1,
        Image = 2,
        Voice = 3,
        Video = 4,
        Location = 5,
        Link = 6,
        Event = 7,
        Music=8,
        News=9,
        shortvideo=10,
        Others = 0
    }
}
