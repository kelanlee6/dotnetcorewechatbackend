﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Models.Enum;

namespace Models
{
    public class MessageModel
    {
        //Public Data
        public string ToUserName { get; set; }
        public string FromUserName { get; set; }
        public DateTime CreateTime { get; set; }
        public MessageType MsgType { get; set; }
        public string MsgId { get; set; }
        public XmlDocument SourceData { get; set; }
        //Test
        public string Content { get; set; }

        //image
        public string PicUrl { get; set; }
        public string MediaId { get; set; }

        //Voice Video
        public string Format { get; set; }
        public string ThumbMediaId { get; set; }

        //Location
        public string Location_X { get; set; }
        public string Location_Y { get; set; }
        public string Scale { get; set; }
        public string Label { get; set; }

        //Link
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        
        
        //Event
        public EventType Event { get; set; }
        public string EventKey { get; set; }
        public string Ticket { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Precision { get; set; }

        //Musci
        public string MusicURL { get; set; }
        public string HQMusicUrl { get; set; }

        public MessageModel()
        {
        }

        public MessageModel(XmlDocument xml)
        {
            SourceData = xml;
            var fromUserNameList = xml.GetElementsByTagName("FromUserName");
            for (int i = 0; i < fromUserNameList.Count; i++)
            {
                if (fromUserNameList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    FromUserName = fromUserNameList[i].ChildNodes[0].Value;
                }
            }
            var toUsernameList = xml.GetElementsByTagName("ToUserName");
            for (int i = 0; i < toUsernameList.Count; i++)
            {
                if (toUsernameList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    ToUserName = toUsernameList[i].ChildNodes[0].Value;
                }
            }
            CreateTime = GetTime(xml.GetElementsByTagName("CreateTime")[0].InnerText);

            var msgTypeList = xml.GetElementsByTagName("MsgType");
            for (int i = 0; i < msgTypeList.Count; i++)
            {
                if (msgTypeList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    MsgType = GetMessageType(msgTypeList[i].ChildNodes[0].Value);
                }
            }

            var msgIdData = xml.GetElementsByTagName("MsgId");
            if (msgIdData.Count > 0)
            {
                MsgId = xml.GetElementsByTagName("MsgId")[0].InnerText;
            }


            var contentList = xml.GetElementsByTagName("Content");
            for (int i = 0; i < contentList.Count; i++)
            {
                if (contentList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Content = contentList[i].ChildNodes[0].Value;
                }
            }


            var picUrlList = xml.GetElementsByTagName("PicUrl");
            for (int i = 0; i < picUrlList.Count; i++)
            {
                if (picUrlList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    PicUrl = picUrlList[i].ChildNodes[0].Value;
                }
            }

            var mediaIdList = xml.GetElementsByTagName("MediaId");
            for (int i = 0; i < mediaIdList.Count; i++)
            {
                if (mediaIdList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    MediaId = mediaIdList[i].ChildNodes[0].Value;
                }
            }


            var formatList = xml.GetElementsByTagName("Format");
            for (int i = 0; i < formatList.Count; i++)
            {
                if (formatList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Format = formatList[i].ChildNodes[0].Value;
                }
            }

            var thumbMediaIdList = xml.GetElementsByTagName("ThumbMediaId");
            for (int i = 0; i < thumbMediaIdList.Count; i++)
            {
                if (thumbMediaIdList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    ThumbMediaId = thumbMediaIdList[i].ChildNodes[0].Value;
                }
            }

            var locationXList = xml.GetElementsByTagName("Location_X");
            for (int i = 0; i < locationXList.Count; i++)
            {
                if (locationXList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Location_X = locationXList[i].ChildNodes[0].Value;
                }
            }

            var locationYList = xml.GetElementsByTagName("Location_Y");
            for (int i = 0; i < locationYList.Count; i++)
            {
                if (locationYList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Location_Y = locationYList[i].ChildNodes[0].Value;
                }
            }


            var scaleList = xml.GetElementsByTagName("Scale");
            for (int i = 0; i < scaleList.Count; i++)
            {
                if (scaleList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Scale = scaleList[i].ChildNodes[0].Value;
                }
            }

            var labelList = xml.GetElementsByTagName("Label");
            for (int i = 0; i < labelList.Count; i++)
            {
                if (labelList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Label = labelList[i].ChildNodes[0].Value;
                }
            }

            var titleList = xml.GetElementsByTagName("Title");
            for (int i = 0; i < titleList.Count; i++)
            {
                if (titleList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Title = titleList[i].ChildNodes[0].Value;
                }
            }
            var descriptionList = xml.GetElementsByTagName("Description");
            for (int i = 0; i < descriptionList.Count; i++)
            {
                if (descriptionList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Description = descriptionList[i].ChildNodes[0].Value;
                }
            }
            var urlList = xml.GetElementsByTagName("Url");
            for (int i = 0; i < urlList.Count; i++)
            {
                if (urlList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Url = urlList[i].ChildNodes[0].Value;
                }
            }

            var eventList = xml.GetElementsByTagName("Event");
            for (int i = 0; i < eventList.Count; i++)
            {
                if (eventList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Event = GetEnentType(eventList[i].ChildNodes[0].Value);
                }
            }

            var eventKeyList = xml.GetElementsByTagName("EventKey");
            for (int i = 0; i < eventKeyList.Count; i++)
            {
                if (eventKeyList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    EventKey = eventKeyList[i].ChildNodes[0].Value;
                }
            }
            var ticketList = xml.GetElementsByTagName("Ticket");
            for (int i = 0; i < ticketList.Count; i++)
            {
                if (ticketList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Ticket = ticketList[i].ChildNodes[0].Value;
                }
            }

            var latitudetList = xml.GetElementsByTagName("Latitude");
            for (int i = 0; i < latitudetList.Count; i++)
            {
                if (latitudetList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Latitude = latitudetList[i].ChildNodes[0].Value;
                }
            }
            var longitudeList = xml.GetElementsByTagName("Longitude");
            for (int i = 0; i < longitudeList.Count; i++)
            {
                if (longitudeList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Longitude = urlList[i].ChildNodes[0].Value;
                }
            }
            var precisionList = xml.GetElementsByTagName("Precision");
            for (int i = 0; i < precisionList.Count; i++)
            {
                if (precisionList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    Precision = precisionList[i].ChildNodes[0].Value;
                }
            }

            var musicUrlList = xml.GetElementsByTagName("MusicURL");
            for (int i = 0; i < musicUrlList.Count; i++)
            {
                if (musicUrlList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    MusicURL = musicUrlList[i].ChildNodes[0].Value;
                }
            }

            var hqMusicUrlList = xml.GetElementsByTagName("HQMusicUrl");
            for (int i = 0; i < precisionList.Count; i++)
            {
                if (hqMusicUrlList[i].ChildNodes[0].NodeType == System.Xml.XmlNodeType.CDATA)
                {
                    HQMusicUrl = hqMusicUrlList[i].ChildNodes[0].Value;
                }
            }

        }

        public MessageType GetMessageType(string str)
        {
            switch (str.ToLower())
            {
                case "text":
                    return MessageType.Text;
                case "image":
                    return MessageType.Image;
                case "voice":
                    return MessageType.Voice;
                case "video":
                    return MessageType.Video;
                case "location":
                    return MessageType.Location;
                case "link":
                    return MessageType.Link;
                case "event":
                    return MessageType.Event;
                case "shortvideo":
                    return MessageType.shortvideo;
                case "music":
                    return MessageType.Music;
                case "news":
                    return MessageType.News;
                default:
                    return MessageType.Others;
            }
        }
        public EventType GetEnentType(string str)
        {
            switch (str.ToLower())
            {
                case "subscribe":
                    return EventType.subscribe;
                case "unsubscribe":
                    return EventType.unsubscribe;
                case "location":
                    return EventType.LOCATION;
                case "click":
                    return EventType.CLICK;
                case "view":
                    return EventType.VIEW;
                default:
                    return EventType.others;

            }
        }

        public string ToXml()
        {
            var result = string.Empty;
            result = "<xml>" +
                     $"<ToUserName><![CDATA[{ToUserName}]]></ToUserName>" +
                     $"<FromUserName><![CDATA[{FromUserName}]]></FromUserName>" +
                     $"<CreateTime>{ConvertDateTimeInt(CreateTime)}</CreateTime>" +
                     $"<MsgType><![CDATA[{MsgType.ToString().ToLower()}]]></MsgType>";
            switch (MsgType)
            {
                case MessageType.Text:
                    result += $"<Content><![CDATA[{Content}]]></Content></xml>";
                    break;
                case MessageType.Image:
                    result +=$"<Image><MediaId><![CDATA[{MediaId}]]></MediaId></Image></xml>";
                    break;
                case MessageType.Voice:
                    result += $"<Voice><MediaId><![CDATA[{MediaId}]]></MediaId></Voice></xml>";
                    break;
                case MessageType.Video:
                    result+=$"<Video><MediaId><![CDATA[{MediaId}]]></MediaId><Title><![CDATA[{Title}]]></Title><Description><![CDATA[{Description}]]></Description></Video></xml>";
                    break;
                case MessageType.Music:
                    result+=$"<Music><Title><![CDATA[{Title}]]></Title><Description><![CDATA[{Description}]]></Description><MusicUrl><![CDATA[{MusicURL}]]></MusicUrl><HQMusicUrl><![CDATA[{HQMusicUrl}]]></HQMusicUrl><humbMediaId><![CDATA[{ThumbMediaId}]]></ThumbMediaId></Music></xml>";
                    break;
                case MessageType.News:
                    return string.Empty;
                    break;
                    case MessageType.shortvideo:
                    result += $"<MediaId><![CDATA[{MediaId}]]></MediaId><ThumbMediaId><![CDATA[{ThumbMediaId}]]></ThumbMediaId></xml>";
                        break;
                case MessageType.Location:
                    return string.Empty;
                case MessageType.Link:
                    return string.Empty;
                case MessageType.Event:
                    return string.Empty;
                case MessageType.Others:
                    return string.Empty;
                default:
                    return string.Empty;
            }
            return result;
        }

        public MessageModel CreateResponseTextMessage(string context)
        {
            var msg=new MessageModel();
            msg.ToUserName = FromUserName;
            msg.FromUserName = ToUserName;
            msg.MsgType=MessageType.Text;
            msg.CreateTime=DateTime.Now;
            msg.Content = context;
            return msg;
        }

        public MessageModel CreateRepeatResponeMessage()
        {
            var msg=new MessageModel(SourceData);
            var fromUser = msg.FromUserName;
            msg.FromUserName = msg.ToUserName;
            msg.ToUserName = fromUser;
            return msg;
        }

        private DateTime GetTime(string timeStamp)
        {
            DateTime dtStart = new DateTime(1970, 1, 1);
            long lTime = long.Parse(timeStamp + "0000000");
            TimeSpan toNow = new TimeSpan(lTime);
            return dtStart.Add(toNow);
        }
        private int ConvertDateTimeInt(DateTime time)
        {
            System.DateTime startTime = new DateTime(1970, 1, 1);
            return (int)(time - startTime).TotalSeconds;
        }
    }

    
}
