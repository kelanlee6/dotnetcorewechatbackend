﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Database.Entities
{
    public class VoiceMessage : MessageBase
    {
        public string MediaId { get; set; }
        public string Format { get; set; }
        public string Recognition { get; set; }

    }
}
