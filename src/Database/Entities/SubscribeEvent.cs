﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Database.Entities
{
    public class SubscribeEvent : EventBase
    {
        public string EventKey { get; set; }
        public string Ticket { get; set; }
    }
}
