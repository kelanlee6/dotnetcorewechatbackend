﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Database.Entities
{
    public class ImageMessage : MessageBase
    {
        public string PicUrl { get; set; }
        public string MediaId { get; set; }
    }
}
