﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Database.Entities
{
    public class LocationEvent:EventBase
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Precision { get; set; }
    }
}
