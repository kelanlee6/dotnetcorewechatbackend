﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Database.Entities
{
    public class ShortVideoMessage:MessageBase
    {
        public string MediaId { get; set; }
        public string ThumbMediaId { get; set; }

    }
}
