﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Database.Entities
{
    public class WechatToken
    {
        [Key]
        public string AppId { get; set; }
        public string Secret { get; set; }
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public DateTime expired_date { get; set; }

    }
}
