﻿using System.Reflection;

namespace Database.Entities
{
    public class EntityBase
    {
        public void Transfer<T>(T model)
        {
            foreach (var property in typeof(T).GetProperties())
            {
                var resultPropertyInfo = this.GetType().GetProperty(property.Name);
                resultPropertyInfo?.SetValue(this, property.GetValue(model));
            }
            
        }
    }
}
