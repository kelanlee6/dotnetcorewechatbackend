﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Database.Entities
{
    public class WechatUser : EntityBase
    {
        [Key]
        public string openid { get; set; }

        public int subscribe { get; set; }
        public string nickname { get; set; }
        public int sex { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string province { get; set; }
        public string language { get; set; }
        public string headimgurl { get; set; }
        public long subscribe_time { get; set; }
        public string unionid { get; set; }
        public string remark { get; set; }
        public int groupid { get; set; }
        public string tagid_listStr
        {
            get
            {
                return tagid_listStrTemp ?? (listTemp == null?string.Empty:JsonConvert.SerializeObject(listTemp) );
            }
            set { tagid_listStrTemp = value; }
        }

        [NotMapped]
        public List<int> tagid_list
        {
            get { return listTemp ?? (tagid_listStrTemp == null?new List<int>(): JsonConvert.DeserializeObject<List<int>>(tagid_listStrTemp)); }
            set { listTemp = value; }
        }

        private List<int> listTemp { get; set; }
        private string tagid_listStrTemp;
    };

    }
