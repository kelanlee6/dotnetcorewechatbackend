﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Utility;

namespace Database
{
    public class WechatDbContextFactory: IDbContextFactory<WechatDbContext>
    {
        public WechatDbContext Create(DbContextFactoryOptions options)
        {
            var optionsBuilder = new DbContextOptionsBuilder<WechatDbContext>();
            string path = string.Empty;
            switch (ConfigHelper.DataBaseType.ToLower())
            {
                case "sqlserver":
                    var connectString = ConfigHelper.SqlServerConnectionString;
                    optionsBuilder.UseSqlServer(connectString);
                    break;
                default:
                    try
                    {
                        path = "Filename=" + ConfigHelper.SqliteDatabasePath;
                    }
                    catch (Exception e)
                    {
                        path = "Filename=./wechat.db";
                    }
                    optionsBuilder.UseSqlite(path);
                    break;
            }
            return new WechatDbContext(optionsBuilder.Options);
        }
    }
}
