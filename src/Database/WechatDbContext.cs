﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace Database
{
    public class WechatDbContext : DbContext
    {

        public WechatDbContext(DbContextOptions<WechatDbContext> options) : base(options)
        {
        }
        public DbSet<WechatUser> WechatUser { get; set; }
        public DbSet<MenuEvent> ClickEvent { get; set; }
        public DbSet<ImageMessage> ImageMessage { get; set; }
        public DbSet<LinkMessage> LinkMessage { get; set; }
        public DbSet<LocationEvent> LocationEvent { get; set; }
        public DbSet<LocationMessage> LocationMessage { get; set; }
        public DbSet<ShortVideoMessage> ShortVideoMessage { get; set; }
        public DbSet<SubscribeEvent> SubscribeEvent { get; set; }
        public DbSet<TextMessage> TextMessage { get; set; }
        public DbSet<VideoMessage> VideoMessage { get; set; }
        public DbSet<VoiceMessage> VoiceMessage { get; set; }
        public DbSet<WechatToken> WechatToken { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.Entity<WechatUser>().ToTable("WechatUser").HasKey(c => c.openid);
            //builder.Entity<MenuEvent>().ToTable("MenuEvent").HasKey(m => m.Id);
            //builder.Entity<ImageMessage>().ToTable("ImageMessage").HasKey(m => m.Id);
            //builder.Entity<LinkMessage>().ToTable("LinkMessage").HasKey(m => m.Id);
            //builder.Entity<LocationEvent>().ToTable("LocationEvent").HasKey(m => m.Id);
            //builder.Entity<LocationMessage>().ToTable("LocationMessage").HasKey(m => m.Id);
            //builder.Entity<ShortVideoMessage>().ToTable("ShortVideoMessage").HasKey(m => m.Id);
            //builder.Entity<SubscribeEvent>().ToTable("SubscribeEvent").HasKey(m => m.Id);
            //builder.Entity<TextMessage>().ToTable("TextMessage").HasKey(m => m.Id);
            //builder.Entity<VideoMessage>().ToTable("VideoMessage").HasKey(m => m.Id);
            //builder.Entity<VoiceMessage>().ToTable("VoiceMessage").HasKey(m => m.Id);
            //builder.Entity<WechatToken>().ToTable("WechatToken").HasKey(m => m.AppId);
            builder.Entity<WechatUser>().ToTable("WechatUser");
            builder.Entity<MenuEvent>().ToTable("MenuEvent");
            builder.Entity<ImageMessage>().ToTable("ImageMessage");
            builder.Entity<LinkMessage>().ToTable("LinkMessage");
            builder.Entity<LocationEvent>().ToTable("LocationEvent");
            builder.Entity<LocationMessage>().ToTable("LocationMessage");
            builder.Entity<ShortVideoMessage>().ToTable("ShortVideoMessage");
            builder.Entity<SubscribeEvent>().ToTable("SubscribeEvent");
            builder.Entity<TextMessage>().ToTable("TextMessage");
            builder.Entity<VideoMessage>().ToTable("VideoMessage");
            builder.Entity<VoiceMessage>().ToTable("VoiceMessage");
            builder.Entity<WechatToken>().ToTable("WechatToken");
            base.OnModelCreating(builder);
        }
    }
}
