﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class UpdateWechatUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "subscribe_time",
                table: "WechatUser",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "subscribe",
                table: "WechatUser",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "sex",
                table: "WechatUser",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "groupid",
                table: "WechatUser",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "subscribe_time",
                table: "WechatUser",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "subscribe",
                table: "WechatUser",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "sex",
                table: "WechatUser",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "groupid",
                table: "WechatUser",
                nullable: true);
        }
    }
}
