﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Database;

namespace Database.Migrations
{
    [DbContext(typeof(WechatDbContext))]
    partial class WechatDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Database.Entities.ImageMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateTime");

                    b.Property<string>("FromUserName");

                    b.Property<string>("MediaId");

                    b.Property<string>("MsgId");

                    b.Property<int>("MsgType");

                    b.Property<string>("PicUrl");

                    b.Property<string>("ToUserName");

                    b.HasKey("Id");

                    b.ToTable("ImageMessage");
                });

            modelBuilder.Entity("Database.Entities.LinkMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateTime");

                    b.Property<string>("Description");

                    b.Property<string>("FromUserName");

                    b.Property<string>("MsgId");

                    b.Property<int>("MsgType");

                    b.Property<string>("Title");

                    b.Property<string>("ToUserName");

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.ToTable("LinkMessage");
                });

            modelBuilder.Entity("Database.Entities.LocationEvent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateTime");

                    b.Property<int>("Event");

                    b.Property<string>("FromUserName");

                    b.Property<string>("Latitude");

                    b.Property<string>("Longitude");

                    b.Property<int>("MsgType");

                    b.Property<string>("Precision");

                    b.Property<string>("ToUserName");

                    b.HasKey("Id");

                    b.ToTable("LocationEvent");
                });

            modelBuilder.Entity("Database.Entities.LocationMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateTime");

                    b.Property<string>("FromUserName");

                    b.Property<string>("Label");

                    b.Property<string>("Location_X");

                    b.Property<string>("Location_Y");

                    b.Property<string>("MsgId");

                    b.Property<int>("MsgType");

                    b.Property<string>("Scale");

                    b.Property<string>("ToUserName");

                    b.HasKey("Id");

                    b.ToTable("LocationMessage");
                });

            modelBuilder.Entity("Database.Entities.MenuEvent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateTime");

                    b.Property<int>("Event");

                    b.Property<string>("EventKey");

                    b.Property<string>("FromUserName");

                    b.Property<int>("MsgType");

                    b.Property<string>("ToUserName");

                    b.HasKey("Id");

                    b.ToTable("MenuEvent");
                });

            modelBuilder.Entity("Database.Entities.ShortVideoMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateTime");

                    b.Property<string>("FromUserName");

                    b.Property<string>("MediaId");

                    b.Property<string>("MsgId");

                    b.Property<int>("MsgType");

                    b.Property<string>("ThumbMediaId");

                    b.Property<string>("ToUserName");

                    b.HasKey("Id");

                    b.ToTable("ShortVideoMessage");
                });

            modelBuilder.Entity("Database.Entities.SubscribeEvent", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateTime");

                    b.Property<int>("Event");

                    b.Property<string>("EventKey");

                    b.Property<string>("FromUserName");

                    b.Property<int>("MsgType");

                    b.Property<string>("Ticket");

                    b.Property<string>("ToUserName");

                    b.HasKey("Id");

                    b.ToTable("SubscribeEvent");
                });

            modelBuilder.Entity("Database.Entities.TextMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<DateTime>("CreateTime");

                    b.Property<string>("FromUserName");

                    b.Property<string>("MsgId");

                    b.Property<int>("MsgType");

                    b.Property<string>("ToUserName");

                    b.HasKey("Id");

                    b.ToTable("TextMessage");
                });

            modelBuilder.Entity("Database.Entities.VideoMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateTime");

                    b.Property<string>("FromUserName");

                    b.Property<string>("MediaId");

                    b.Property<string>("MsgId");

                    b.Property<int>("MsgType");

                    b.Property<string>("ThumbMediaId");

                    b.Property<string>("ToUserName");

                    b.HasKey("Id");

                    b.ToTable("VideoMessage");
                });

            modelBuilder.Entity("Database.Entities.VoiceMessage", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateTime");

                    b.Property<string>("Format");

                    b.Property<string>("FromUserName");

                    b.Property<string>("MediaId");

                    b.Property<string>("MsgId");

                    b.Property<int>("MsgType");

                    b.Property<string>("Recognition");

                    b.Property<string>("ToUserName");

                    b.HasKey("Id");

                    b.ToTable("VoiceMessage");
                });

            modelBuilder.Entity("Database.Entities.WechatToken", b =>
                {
                    b.Property<string>("AppId");

                    b.Property<string>("Secret");

                    b.Property<string>("access_token");

                    b.Property<DateTime>("expired_date");

                    b.Property<int>("expires_in");

                    b.HasKey("AppId");

                    b.ToTable("WechatToken");
                });

            modelBuilder.Entity("Database.Entities.WechatUser", b =>
                {
                    b.Property<string>("openid");

                    b.Property<string>("city");

                    b.Property<string>("country");

                    b.Property<int>("groupid");

                    b.Property<string>("headimgurl");

                    b.Property<string>("language");

                    b.Property<string>("nickname");

                    b.Property<string>("province");

                    b.Property<string>("remark");

                    b.Property<int>("sex");

                    b.Property<int>("subscribe");

                    b.Property<long>("subscribe_time");

                    b.Property<string>("tagid_listStr");

                    b.Property<string>("unionid");

                    b.HasKey("openid");

                    b.ToTable("WechatUser");
                });
        }
    }
}
