﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Database.Migrations
{
    public partial class InitDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ImageMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    FromUserName = table.Column<string>(nullable: true),
                    MediaId = table.Column<string>(nullable: true),
                    MsgId = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    PicUrl = table.Column<string>(nullable: true),
                    ToUserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageMessage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LinkMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FromUserName = table.Column<string>(nullable: true),
                    MsgId = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    ToUserName = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinkMessage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LocationEvent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    Event = table.Column<int>(nullable: false),
                    FromUserName = table.Column<string>(nullable: true),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    Precision = table.Column<string>(nullable: true),
                    ToUserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationEvent", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LocationMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    FromUserName = table.Column<string>(nullable: true),
                    Label = table.Column<string>(nullable: true),
                    Location_X = table.Column<string>(nullable: true),
                    Location_Y = table.Column<string>(nullable: true),
                    MsgId = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    Scale = table.Column<string>(nullable: true),
                    ToUserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationMessage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MenuEvent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    Event = table.Column<int>(nullable: false),
                    EventKey = table.Column<string>(nullable: true),
                    FromUserName = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    ToUserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuEvent", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShortVideoMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    FromUserName = table.Column<string>(nullable: true),
                    MediaId = table.Column<string>(nullable: true),
                    MsgId = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    ThumbMediaId = table.Column<string>(nullable: true),
                    ToUserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShortVideoMessage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubscribeEvent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    Event = table.Column<int>(nullable: false),
                    EventKey = table.Column<string>(nullable: true),
                    FromUserName = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    Ticket = table.Column<string>(nullable: true),
                    ToUserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubscribeEvent", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TextMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Content = table.Column<string>(nullable: true),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    FromUserName = table.Column<string>(nullable: true),
                    MsgId = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    ToUserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TextMessage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VideoMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    FromUserName = table.Column<string>(nullable: true),
                    MediaId = table.Column<string>(nullable: true),
                    MsgId = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    ThumbMediaId = table.Column<string>(nullable: true),
                    ToUserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoMessage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VoiceMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateTime = table.Column<DateTime>(nullable: false),
                    Format = table.Column<string>(nullable: true),
                    FromUserName = table.Column<string>(nullable: true),
                    MediaId = table.Column<string>(nullable: true),
                    MsgId = table.Column<string>(nullable: true),
                    MsgType = table.Column<int>(nullable: false),
                    Recognition = table.Column<string>(nullable: true),
                    ToUserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoiceMessage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WechatToken",
                columns: table => new
                {
                    AppId = table.Column<string>(nullable: false),
                    Secret = table.Column<string>(nullable: true),
                    access_token = table.Column<string>(nullable: true),
                    expired_date = table.Column<DateTime>(nullable: false),
                    expires_in = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WechatToken", x => x.AppId);
                });

            migrationBuilder.CreateTable(
                name: "WechatUser",
                columns: table => new
                {
                    openid = table.Column<string>(nullable: false),
                    city = table.Column<string>(nullable: true),
                    country = table.Column<string>(nullable: true),
                    groupid = table.Column<string>(nullable: true),
                    headimgurl = table.Column<string>(nullable: true),
                    language = table.Column<string>(nullable: true),
                    nickname = table.Column<string>(nullable: true),
                    province = table.Column<string>(nullable: true),
                    remark = table.Column<string>(nullable: true),
                    sex = table.Column<string>(nullable: true),
                    subscribe = table.Column<string>(nullable: true),
                    subscribe_time = table.Column<string>(nullable: true),
                    tagid_listStr = table.Column<string>(nullable: true),
                    unionid = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WechatUser", x => x.openid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImageMessage");

            migrationBuilder.DropTable(
                name: "LinkMessage");

            migrationBuilder.DropTable(
                name: "LocationEvent");

            migrationBuilder.DropTable(
                name: "LocationMessage");

            migrationBuilder.DropTable(
                name: "MenuEvent");

            migrationBuilder.DropTable(
                name: "ShortVideoMessage");

            migrationBuilder.DropTable(
                name: "SubscribeEvent");

            migrationBuilder.DropTable(
                name: "TextMessage");

            migrationBuilder.DropTable(
                name: "VideoMessage");

            migrationBuilder.DropTable(
                name: "VoiceMessage");

            migrationBuilder.DropTable(
                name: "WechatToken");

            migrationBuilder.DropTable(
                name: "WechatUser");
        }
    }
}
